package sbu.cs.parser.html;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class tester {
    public static void main(String[] args){
        String str="<html>\n" +
                "<body>\n" +
                "\n" +
                "<h1>The input formaction attribute</h1>\n" +
                "\n" +
                "<p>The formaction attribute specifies the URL of a file that will process the input when the form is submitted.</p>\n" +
                "\n" +
                "<form action=\"/action_page.php\">\n" +
                "  <label for=\"fname\">First name:</label>\n" +
                "  </input type=\"text\" id=\"fname\" name=\"fname\"></br></br>\n" +
                "  <label for=\"lname\">Last name:</label>\n" +
                "  </input type=\"text\" id=\"lname\" name=\"lname\"></br></br>\n" +
                "  </input type=\"submit\" value=\"Submit\">\n" +
                "  </input type=\"submit\" formaction=\"/action_page2.php\" value=\"Submit as Admin\">\n" +
                "</form>\n" +
                "\n" +
                "</body>\n" +
                "</html>";
        Node node=HTMLParser.parse(str);
        System.out.println(node.getChildren().get(0).getChildren().get(2).getChildren().get(2));
//        System.out.println(node);
    }



    public static Map<String,String> attributeExtractor(String str){
        Pattern pattern=Pattern.compile("\\S+=\\\".*?\\\"");
        Matcher matcher=pattern.matcher(str);
        Map<String,String> attributes=new HashMap<>();
        while (matcher.find()){
            String attribute=matcher.group();
            String[] extracted=attribute.split("=");
            attributes.put(extracted[0],extracted[1].substring(1,extracted[1].length()-1));
        }
        return attributes;
    }

    /**
     * This function extracts subcategories that are not tags.
     * @param str input string
     * @return returns  string that is not a tag
     */
    public static String getFirstStringNotTagged(String str){
        StringBuilder stringBuilder=new StringBuilder();
        int index=0;
        while (str.charAt(index)!='<'){
            stringBuilder.append(str.charAt(index));
            index++;
            if(str.charAt(index-1)=='\"'&&str.charAt(index)=='<'&&str.charAt(index+1)=='\"'){
                stringBuilder.append("<\"");
                index+=2;
            }
        }
        return stringBuilder.toString();
    }

    private static String getFirstStringTag(String str){
        Pattern pattern=Pattern.compile("(<.*?>)");
        Matcher matcher = pattern.matcher(str);
        String tagString=null;
        if(matcher.find()){
            tagString=matcher.group();
        }
        return tagString;
    }

    private static String nodeNameExtractor(String str){
        StringBuilder tagName=new StringBuilder();
        for(int i=0;i<str.length();i++){
            if(str.charAt(i)==' '||str.charAt(i)=='>'){
                return tagName.toString();
            }
            if(str.charAt(i)!='<'&&str.charAt(i)!='/'){
                tagName.append(str.charAt(i));
            }
        }
        return tagName.toString();
    }

    private static String insideExtractor(String str,String tag,String tagName){
        StringBuilder insideText=new StringBuilder();
        String closeTag="</"+tagName+">";
        int opened=1;
        int closed=0;
        str=str.substring(tag.length()).trim();
        while (opened>closed){
            if(str.charAt(0)!='<'){
                String notTag=getFirstStringNotTagged(str);
                insideText.append(notTag);
                str=str.substring(notTag.length()).trim();
            }else{
                String newTag=getFirstStringTag(str);
                String newTagName=nodeNameExtractor(newTag);
                if(tagName.equals(newTagName)){
                    if(newTag.equals(closeTag)){
                        closed++;
                        if(closed!=opened){
                            insideText.append(newTag);
                            str=str.substring(newTag.length()).trim();
                        }
                    }else{
                        opened++;
                        insideText.append(newTag);
                        str=str.substring(newTag.length()).trim();
                    }
                }else{
                    insideText.append(newTag);
                    str=str.substring(newTag.length()).trim();
                }
            }
        }
        return insideText.toString();
    }
}
