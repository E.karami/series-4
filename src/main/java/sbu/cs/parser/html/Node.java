package sbu.cs.parser.html;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Node implements NodeInterface {
    private String nodeName;
    private Map<String ,String> attributes;
    private String inside;
    private List<Node> children;
    private TagType type;

    //constructors

    public Node() {
        this.nodeName=null;
        this.attributes=new HashMap<>();
        this.inside=null;
        this.children=new ArrayList<>();
        this.type=TagType.unknown;
    }

    public Node(String nodeName, TagType type, Map<String, String> attributes, String inside, List<Node> children) {
        this.nodeName = nodeName;
        this.attributes = new HashMap<>(attributes);
        this.inside = inside;
        this.children = new ArrayList<>(children);
        this.type=type;
    }

    //getters and setters


    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public Map<String, String> getAttributes() {
        return attributes;
    }

    public void setAttributes(Map<String, String> attributes) {
        this.attributes = new HashMap<>(attributes);
    }

    public String getInside() {
        return inside;
    }

    public void setInside(String inside) {
        this.inside = inside;
    }

    public void setChildren(List<Node> children) {
        if(children==null){
            this.children=null;
        }else{
            this.children = new ArrayList<>(children);
        }

    }

    public TagType getType() {
        return type;
    }

    public void setType(TagType type) {
        this.type = type;
    }

    //some additional function

    public void addChildren(Node node){
        this.children.add(node);
    }

    /*
    * this function will return all that exists inside a tag
    * for example for <html><body><p>hi</p></body></html>, if we are on
    * html tag this function will return <body><p1>hi</p1></body> and if we are on
    * body tag this function will return <p1>hi</p1> and if we are on
    * p tag this function will return hi
    * if there is nothing inside tag then null will be returned
     */
    @Override
    public String getStringInside() {

        if(this.inside==null||this.inside.equals("")){
            return null;
        }else{
            return this.inside;
        }
    }

    /*
    *
     */
    @Override
    public List<Node> getChildren() {
        return this.children;
    }

    /*
    * in html tags all attributes are in key value shape. this function will get a attribute key
    * and return it's value as String.
    * for example <img src="img.png" width="400" height="500">
     */
    @Override
    public String getAttributeValue(String key) {
        return this.attributes.get(key);
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder=new StringBuilder();
        if(this.getType()==TagType.ClosingTag){
            stringBuilder.append("</").append(this.nodeName);
            if(this.attributes.size()!=0){
                stringBuilder.append(" ");
                for(String str:this.attributes.keySet()){
                    String key = str.toString();
                    String value = this.attributes.get(str).toString();
                    stringBuilder.append(key + "=\"" + value+"\" ");
                }
            }
            stringBuilder.append(">");
            return stringBuilder.toString();
        }else{
            stringBuilder.append("<").append(this.nodeName);
            if(this.attributes.size()!=0){
                stringBuilder.append(" ");
                for(String str:this.attributes.keySet()){
                    String key = str.toString();
                    String value = this.attributes.get(str).toString();
                    stringBuilder.append(key + "=\"" + value+"\" ");
                }
            }
            stringBuilder.append(">");
            if(this.inside!=null){
                stringBuilder.append(this.inside);
            }
            stringBuilder.append("</").append(this.nodeName).append(">");
            return stringBuilder.toString();
        }
    }
}
