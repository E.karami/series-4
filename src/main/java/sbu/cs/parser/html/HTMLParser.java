package sbu.cs.parser.html;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HTMLParser {

    /*
    * this function will get a String and returns a Dom object
     */
    public static Node parse(String document) {
        Node node=new Node();
        document=deleteBreakLine(document);
        document=document.trim();
        String tagString=null;
        tagString=getFirstStringTag(document);
        node.setAttributes(attributeExtractor(tagString));
        node.setNodeName(nodeNameExtractor(tagString));
        node.setType(tagTypeExtractor(tagString));
        if(node.getType()==TagType.ClosingTag){
            node.setChildren(null);
            node.setInside(null);
            String stringAfterOfTag=getFirstStringNotTagged(document.substring(tagString.length()).trim());
            document=document.substring(tagString.length()+stringAfterOfTag.length());
            return node;
        }else{
            String inside=insideExtractor(document,tagString,node.getNodeName());
            node.setInside(inside);
            if(inside!=null){
                inside=inside.substring(getFirstStringNotTagged(inside).length());
                node.setChildren(HTMLParser.parseChildrenNodes(inside));
            }else{
                node.setChildren(null);
            }
        }
        return node;
    }

    /**
     * this function gets inside string of a tag and returns its children nodes
     * @param document inside string of A tag
     * @return list ao nodes
     */
    private static List<Node> parseChildrenNodes(String document){
        List<Node> nodes=new ArrayList<>();
        document=document.trim();
        String tagString=null;
        while (isNewTagAvailable(document)){
            Node node=new Node();
            tagString=getFirstStringTag(document);
            node.setAttributes(attributeExtractor(tagString));
            node.setNodeName(nodeNameExtractor(tagString));
            node.setType(tagTypeExtractor(tagString));
            if(node.getType()==TagType.ClosingTag){
                node.setChildren(null);
                node.setInside(null);
                String stringAfterOfTag=getFirstStringNotTagged(document.substring(tagString.length()).trim());
                document=document.substring(tagString.length()+stringAfterOfTag.length());
                nodes.add(node);
            }else{
                String inside=insideExtractor(document,tagString,node.getNodeName());
                node.setInside(inside);
                String all=tagString+inside+"</"+node.getNodeName()+">";
                String stringAfterOfTag=getFirstStringNotTagged(document.substring(all.length()).trim());
                document=document.substring(all.length()+stringAfterOfTag.length());
                if(inside!=null){
                    inside=inside.substring(getFirstStringNotTagged(inside).length());
                    node.setChildren(HTMLParser.parseChildrenNodes(inside));
                }else{
                    node.setChildren(null);
                }
                nodes.add(node);
            }
        }

        if(nodes.size()==0){
            return null;
        }
        return nodes;
    }
    //  regex:  (<.*?>)

    /*
    * a function that will return string representation of dom object.
    * only implement this after all other functions been implemented because this
    * impl is not required for this series of exercises. this is for more score
     */
    public static String toHTMLString(Node root) {
        return root.toString();
    }

    /**
     * this function deletes all break lines and tabs from a JSON String
     * @param str input Json String
     * @return Modified string
     */
    private static String deleteBreakLine(String str) {
        return str.replaceAll("\\r|\\n|\\t", "");
    }

    /**
     * this function gets an String and return string of first tag of it if it is a closing tag
     * elese it returns first part of it
     * @param str input html string
     * @return returns a tag String
     */
    private static String getFirstStringTag(String str){
        Pattern pattern=Pattern.compile("(<.*?>)");
        Matcher matcher = pattern.matcher(str);
        String tagString=null;
        if(matcher.find()){
            tagString=matcher.group();
        }
        return tagString;
    }

    /**
     * this function gets string af a tag and returns its attributes
     * @param str input string
     * @return returns a Map of attributes
     */
    private static Map<String,String> attributeExtractor(String str){
        Pattern pattern=Pattern.compile("\\S+=\\\".*?\\\"");
        Matcher matcher=pattern.matcher(str);
        Map<String,String> attributes=new HashMap<>();
        while (matcher.find()){
            String attribute=matcher.group();
            String[] extracted=attribute.split("=");
            attributes.put(extracted[0],extracted[1].substring(1,extracted[1].length()-1));
        }
        return attributes;
    }

    /**
     * this function gets string af a tag and returns its name
     * @param str input string
     * @return returns a tag name as a string
     */
    private static String nodeNameExtractor(String str){
        StringBuilder tagName=new StringBuilder();
        for(int i=0;i<str.length();i++){
            if(str.charAt(i)==' '||str.charAt(i)=='>'){
                return tagName.toString();
            }
            if(str.charAt(i)!='<'&&str.charAt(i)!='/'){
                tagName.append(str.charAt(i));
            }
        }
        return tagName.toString();
    }

    /**
     * this function gets string af a tag and returns its tagType
     * @param str input string
     * @return returns tagType
     */
    private static TagType tagTypeExtractor(String str){
        if(str.charAt(1)=='/'){
            return TagType.ClosingTag;
        }else{
            return TagType.OpeningTag;
        }
    }

    /**
     * This function is generated to extract inside text of a tag
     * @param str input string
     * @param tag string of outed tag
     * @param tagName name of outed tag
     * @return inside text of a tag
     */
    private static String insideExtractor(String str,String tag,String tagName){
        StringBuilder insideText=new StringBuilder();
        String closeTag="</"+tagName+">";
        int opened=1;
        int closed=0;
        str=str.substring(tag.length()).trim();
        while (opened>closed){
            if(str.charAt(0)!='<'){
                String notTag=getFirstStringNotTagged(str);
                insideText.append(notTag);
                str=str.substring(notTag.length()).trim();
            }else{
                String newTag=getFirstStringTag(str);
                String newTagName=nodeNameExtractor(newTag);
                if(tagName.equals(newTagName)){
                    if(newTag.equals(closeTag)){
                        closed++;
                        if(closed!=opened){
                            insideText.append(newTag);
                            str=str.substring(newTag.length()).trim();
                        }
                    }else{
                        opened++;
                        insideText.append(newTag);
                        str=str.substring(newTag.length()).trim();
                    }
                }else{
                    insideText.append(newTag);
                    str=str.substring(newTag.length()).trim();
                }
            }
        }

        if(insideText.toString().equals("")){
            return null;
        }
        return insideText.toString();
    }

    /**
     * This function extracts subcategories that are not tags.
     * @param str input string
     * @return returns  string that is not a tag
     */
    private static String getFirstStringNotTagged(String str){
        if(str.equals("")){
            return str;
        }
        StringBuilder stringBuilder=new StringBuilder();
        int index=0;
        while (str.charAt(index)!='<'){
            stringBuilder.append(str.charAt(index));
            index++;
            if(str.charAt(index-1)=='\"'&&str.charAt(index)=='<'&&str.charAt(index+1)=='\"'){
                stringBuilder.append("<\"");
                index+=2;
            }
            if(index==str.length()){
                break;
            }
        }
        return stringBuilder.toString();
    }

    /**
     * this function checks the input string include a tag or not
     * @param str input string
     * @return returns result of checking
     */
    private static boolean isNewTagAvailable(String str){
        Pattern pattern=Pattern.compile("(<.*?>)");
        Matcher matcher=pattern.matcher(str);
        return matcher.find();
    }
}
