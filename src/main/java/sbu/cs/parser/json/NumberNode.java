package sbu.cs.parser.json;
/**
 * this class is for handling numbers in Json
 * @param <T> T is Type Parameters that extends java.lang.Number
 */
public class NumberNode <T extends Number> extends Node {
    private T value;

    public NumberNode() {
        super.setKey("");
        this.value = null;
    }

    public NumberNode(String key, T value) {
        this.setKey(key);
        this.value = value;
    }

    @Override
    public String getKey() {
        return  super.getKey() ;
    }

    @Override
    public void setKey(String key) {
        super.setKey(key);
    }

    @Override
    public String getValueString() {
        StringBuilder stringBuilder;
        stringBuilder = new StringBuilder();
        stringBuilder.append(value);
        return stringBuilder.toString();
    }

    @Override
    public String toString() {
        return " \"" +this.getKey()+"\" "+
                ": "+
                this.value+
                " ";
    }
    @Override
    public T getValue() {
        return this.value;
    }

    public void setValue(T value) {
        this.value = value;
    }
}
