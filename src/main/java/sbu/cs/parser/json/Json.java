package sbu.cs.parser.json;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Json implements JsonInterface {
    private List<Node> nodes;

    //constructors
    public Json() {
        this.nodes= new ArrayList<Node>();
    }

    public Json(List<Node> nodes) {
        this.nodes = nodes;
    }

    //getter and setter
    public List<Node> getNodes() {
        return this.nodes;
    }

    public void setNodes(List<Node> nodes) {
        Collections.copy(this.nodes, nodes);
    }

    /**
     * adding a node to Json object
     * @param node input node
     */
    public void addNode(Node node){
        nodes.add(node);
    }

    @Override
    public String getStringValue(String key) {
        /*
        Because nested JSON are supported,
        we use the same original format for searching them as the separation with ":"
        if search field function returns null
         */
        String[] search=key.split(":");
        for(int i=0;i<this.nodes.size();i++){
            if(nodes.get(i).getKey().equals(search[0])){
                if(search.length==1){
                    return nodes.get(i).getValueString();
                }else{
                    if(nodes.get(i) instanceof JsonNode){
                        StringBuilder newSearch=new StringBuilder();
                        for(int j=1;j<search.length-1;j++){
                            newSearch.append(search[j]).append(":");
                        }
                        newSearch.append(search[search.length-1]);
                        return ((JsonNode) nodes.get(i)).getValue().getStringValue(newSearch.toString());
                    }else{
                        return null;
                    }
                }
            }
        }
        return null;
    }

    /**
     * this function is for more score section
     * @param key input key string
     * @return founded node if search filed returns null
     */
    public Object getValue(String key){
        /*
        Because nested JSON are supported,
        we use the same original format for searching them as the separation with ":"
        if search field function returns null
         */
        String[] search=key.split(":");
        for(int i=0;i<this.nodes.size();i++){
            if(nodes.get(i).getKey().equals(search[0])){
                if(search.length==1){
                    return nodes.get(i).getValue();
                }else{
                    if(nodes.get(i) instanceof JsonNode){
                        StringBuilder newSearch=new StringBuilder();
                        for(int j=1;j<search.length-1;j++){
                            newSearch.append(search[j]).append(":");
                        }
                        newSearch.append(search[search.length-1]);
                        return ((JsonNode) nodes.get(i)).getValue().getValue(newSearch.toString());
                    }else{
                        return null;
                    }
                }
            }
        }
        return null;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder=new StringBuilder();
        stringBuilder.append("{");
        for(int i=0;i<nodes.size();i++){
            stringBuilder.append(nodes.get(i));
            if(i!=nodes.size()-1){
                stringBuilder.append(",");
            }else{
                stringBuilder.append("}");
            }
        }
        return stringBuilder.toString();
    }
}
