package sbu.cs.parser.json;

/**
 * this class is for handling inner Jason data in Json
 */
public class JsonNode extends Node {
    private Json value;

    public JsonNode() {
        super.setKey("");
        this.value=null;
    }

    public JsonNode(String key,Json value) {
        this.setKey(key);
        this.value = value;
    }

    @Override
    public String getKey() {
        return super.getKey();
    }

    @Override
    public void setKey(String key) {
        super.setKey(key);
    }

    @Override
    public String getValueString() {
        if(this.value!=null){
            return this.value.toString();
        }
        return null;
    }

    @Override
    public String toString() {
        return " \"" +this.getKey()+"\" "+
                ": "+
                this.value+
                " ";
    }
    @Override
    public Json getValue() {
        return value;
    }

    public void setValue(Json value) {
        this.value = value;
    }
}
