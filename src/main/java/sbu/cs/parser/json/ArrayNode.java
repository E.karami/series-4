package sbu.cs.parser.json;

import java.util.Arrays;

/**
 * this class is for handling arrays in Json
 * @param <T> T is array Type Parameters
 */
public class ArrayNode <T> extends Node {
    private T[] value;

    public ArrayNode() {
        super.setKey("");
        this.value=null;
    }

    public ArrayNode(String key,T[] value) {
        this.setKey(key);
        this.value = value;
    }
    @Override
    public T[] getValue() {
        return value;
    }

    public void setValue(T[] value) {
        this.value = value;
    }



    @Override
    public String getKey() {
        return super.getKey();
    }

    @Override
    public void setKey(String key) {
        super.setKey(key);
    }

    @Override
    public String getValueString() {
        return Arrays.toString(value);
    }

    @Override
    public String toString() {
        if(value instanceof String[]){
            String[] copyArray=null;
            copyArray= (String[]) value.clone();
            for(int i=0;i<copyArray.length;i++){
                copyArray[i]="\""+copyArray[i]+"\"";
            }
            return " \"" +this.getKey()+"\" "+
                    ": "+
                    Arrays.toString(copyArray) ;
        }
        return " \"" +this.getKey()+"\" "+
                ": "+
                Arrays.toString(value) ;
    }
}
