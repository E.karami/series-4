package sbu.cs.parser.json;

/**
 * this class is for handling String data in Json
 */
public class StringNode extends Node {
    private String value;
    public StringNode() {
        super.setKey("");
        this.value = "";
    }

    public StringNode(String key,String value) {
        this.setKey(key);
        this.value = value;
    }

    @Override
    public String getKey() {
        return  super.getKey() ;
    }

    @Override
    public void setKey(String key) {
        super.setKey(key);
    }

    @Override
    public String getValueString() {
        return getValue();
    }

    @Override
    public String toString() {
        return " \"" +this.getKey()+"\" "+
                ":"+
                " \""+this.value+"\" ";
    }
    @Override
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
