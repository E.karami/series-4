package sbu.cs.parser.json;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JsonParser {

    /*
     * this function will get a String and returns a Json object
     */
    public static Json parse(String data)  {
        if(!validation(data)){
            System.err.println("invalid JSON String");
            return null;
        }
        Json json=new Json();
        data=deleteBreakLine(data);
        String[] elements=extractKeyValuePair(data);
        for(String str:elements){
            String[] keyValue=separateKeyFromValue(str);
            String key=stringExtractor(keyValue[0]);
            switch (keyValue[1].charAt(0)){
                case '{':{
                    JsonNode jsonNode=new JsonNode(key,JsonParser.parse(keyValue[1]));
                    json.addNode(jsonNode);
                }
                break;
                case '[':{
                    String[] arrayElements=arrayParser(keyValue[1]);
                    switch (arrayElements[0].charAt(0)){
                        case '{':{
                            ArrayNode<Json>arrayNode=new ArrayNode<>(key,jsonArrayExtractor(arrayElements));
                            json.addNode(arrayNode);
                        }
                        break;
                        case 'f':
                        case 't':{
                            ArrayNode<Boolean> booleanArrayNode=new ArrayNode<>(key,booleanArrayExtractor(arrayElements));
                            json.addNode(booleanArrayNode);
                        }
                        break;
                        case '\"':{
                            ArrayNode<String> stringArrayNode=new ArrayNode<>(key,stringArrayExtractor(arrayElements));
                            json.addNode(stringArrayNode);
                        }
                        break;
                        case 'n':{
                            ArrayNode<Object> nullArrayNode=new ArrayNode<>(key,nullArrayExtractor(arrayElements));
                            json.addNode(nullArrayNode);
                        }
                        break;
                        default:{
                            ArrayNode<Number> numberArrayNode=new ArrayNode<>(key,numberArrayExtractor(arrayElements));
                            json.addNode(numberArrayNode);
                        }
                    }

                }
                break;
                case 'f':
                case 't':{
                    BooleanNode booleanNode=null;
                    try{
                        booleanNode=new BooleanNode(key,booleanExtractor(keyValue[1]));
                    } catch (Exception exception){
                        System.err.println(exception.getMessage());
                        return null;
                    }
                    json.addNode(booleanNode);
                }
                break;
                case '\"':{
                    StringNode stringNode=new StringNode(key,stringExtractor(keyValue[1]));
                    json.addNode(stringNode);
                }
                break;
                case 'n':{
                    NullNode nullNode=null;
                    try{
                        nullNode=new NullNode(key,nullExtractor(keyValue[1]));
                    }catch (Exception ex){
                        System.err.println(ex.getMessage());
                        return null;
                    }
                    json.addNode(nullNode);
                }
                break;
                default:{
                    NumberNode<Number> numberNode=new NumberNode<>(key,numberExtractor(keyValue[1]));
                    json.addNode(numberNode);
                }
            }
        }
        return json;
    }

    /*
     * this function is the opposite of above function. implementing this has no score and
     * is only for those who want to practice more.
     */
    public static String toJsonString(Json data) {
        return data.toString();
    }
    /**
     * this function deletes all break lines and tabs from a JSON String
     * @param str input Json String
     * @return Modified string
     */
    private static String deleteBreakLine(String str) {
        return str.replaceAll("\\r|\\n|\\t", "");
    }

    /**
     * this function extract all <key:value<key:value> sections from JSON String
     * @param data input JSON String
     * @return arrays of string that every member is <key:value>
     */
    private static String[] extractKeyValuePair(String data) {
        Pattern pattern = Pattern.compile("\\\".*?\\\"\\s*:(\\{.*?\\}|\\s*\\[.*?\\]|.*?,|.*})");
        Matcher matcher = pattern.matcher(data);
        List<String> list = new ArrayList<>();
        while (matcher.find()) {
            String subStr=matcher.group();
            if(subStr.charAt(subStr.length()-1)==','){
                subStr=subStr.substring(0,subStr.length()-1);
            }else{
                if(subStr.contains("}")&&!subStr.contains("{")){
                    subStr=subStr.substring(0,subStr.length()-1);
                }
            }
            list.add(subStr);
        }
        return list.toArray(new String[0]);
    }

    //this function Not implemented because I could not find an Appropriate regex for Json validation
    private static boolean validation(String data) {
        return true;
    }

    /**
     * this function gets an string in format <key:value> and breaks it in key and value
     * @param str input String
     * @return return Two-member string array that firt is key and second is value
     */
    private static String[] separateKeyFromValue(String str){
        String[] separated=str.split(":",2);
        for(int i=0;i<separated.length;i++){
            separated[i]=separated[i].trim();
        }
        return separated;
    }

    //every data Extractors

    /**
     * this function modifiers an String element in Json
     * @param str input string in format ""every""
     * @return returns modified sting in format "every"
     */
    private static String stringExtractor(String str){
        return str.substring(1,str.length()-1);
    }

    /**
     * this function modifiers an null element in Json
     * @return an null Object
     */
    private static Object nullExtractor(String str)throws Exception{
        if(!str.equals("null")){
            throw new Exception("Invalid Json String");
        }
        return null;
    }

    /**
     * this function modifiers an boolean element in Json
     * @param str input string
     * @return returns a boolean
     * @throws Exception if input string Is not equivalent with "true" or "false" throws an exception
     */
    private static boolean booleanExtractor(String str) throws Exception{
        if(str.equals("true")){
            return true;
        }
        if(str.equals("false")){
            return false;
        }
        throw new Exception("invalid String");
    }

    /**
     * this function modifiers an Number element in Json
     * @param str input string
     * @return returns a Number
     */
    private static Number numberExtractor(String str){
        try
        {
            return Integer.parseInt(str);
        }
        catch(NumberFormatException e)
        {
            try {
                return Double.parseDouble(str);
            }catch (NumberFormatException ex){
                System.err.println("invalid string format");
            }
        }
        return -1;
    }

    /**
     * this function parses an array elements of a string
     * @param str input string in format "[.*]"
     * @return an string array of each element
     */
    private static String[] arrayParser(String str){
        str=str.substring(1,str.length()-1);
        List<String> list = new ArrayList<>();
        if (str.contains("{")){
            Pattern pattern = Pattern.compile("\\{.*?\\}");
            Matcher matcher = pattern.matcher(str);
            while (matcher.find()) {
                String subStr=matcher.group();
                list.add(subStr.trim());
            }
            return list.toArray(new String[0]);

        }else{
            String[] strings=str.split(",");
            for(int i=0;i<strings.length;i++){
                strings[i]=strings[i].trim();
            }
            return strings;
        }
    }

    /**
     * this function extract Number data from an array
     * @param strings input array string
     * @return array of Numbers
     */
    private static Number[] numberArrayExtractor(String[] strings){
        ArrayList<Number> arrayList=new ArrayList<>();
        for (int i=0;i<strings.length;i++){
            arrayList.add(numberExtractor(strings[i]));
        }
        return arrayList.toArray(new Number[0]);
    }

    /**
     * this function extract String data from an array
     * @param strings input array string
     * @return array of Strings
     */
    private static String[] stringArrayExtractor(String[] strings){
        String[] strings1=new String[strings.length];
        for (int i=0;i<strings.length;i++){
            strings1[i]=stringExtractor(strings[i]);
        }
        return strings1;
    }

    /**
     * this function extract null data from an array
     * @param strings input array string
     * @return array of null Objects
     */
    private static Object[] nullArrayExtractor(String[] strings){
        Object[] objects=new Object[strings.length];
        for (int i=0;i<strings.length;i++){
            try{
                objects[i]=nullExtractor(strings[i]);
            }catch (Exception ex){
                System.err.println("Invalid JSON format");
                return null;
            }
        }
        return objects;
    }
    /**
     * this function extract boolean data from an array
     * @param strings input array string
     * @return array of boolean
     */
    private static Boolean[] booleanArrayExtractor(String[] strings){
        Boolean[] booleans=new Boolean[strings.length];
        for (int i=0;i<strings.length;i++){
            try {
                booleans[i]=booleanExtractor(strings[i]);
            }catch(Exception ex){
                System.err.println("Invalid JSON format");
                return null;
            }
        }
        return booleans;
    }

    /**
     * this function extract json data from an array
     * @param strings input array string
     * @return array of json
     */
    private static Json[] jsonArrayExtractor(String[] strings){
        Json[]jsons=new Json[strings.length];
        for (int i=0;i<strings.length;i++){
            jsons[i]=JsonParser.parse(strings[i]);
        }
        return jsons;
    }

}
