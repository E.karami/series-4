package sbu.cs.parser.json;

/**
 * super class of data holder classes
 */
public abstract class Node {
    private String key;

    public  String getKey() {
        return this.key;
    };

    public void setKey(String key){
        this.key=key;
    }

    public abstract String getValueString();

    public abstract Object getValue();

}
