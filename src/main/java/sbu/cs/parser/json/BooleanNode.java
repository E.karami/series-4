package sbu.cs.parser.json;

/**
 * this class is for handling Boolean data in Json
 */
class BooleanNode extends Node{
    private Boolean value;

    public BooleanNode() {
        super.setKey("");
        this.value = false;
    }

    public BooleanNode(String key,boolean value) {
        this.setKey(key);
        this.value = value;
    }

    @Override
    public String getKey() {
        return  super.getKey();
    }

    @Override
    public void setKey(String key) {
        super.setKey(key);
    }

    @Override
    public String getValueString() {
        StringBuilder stringBuilder=new StringBuilder();
        stringBuilder.append(value);
        return stringBuilder.toString() ;
    }

    @Override
    public String toString() {
        return " \"" +this.getKey()+"\" "+
                ": "+
                this.value+
                " ";
    }

    @Override
    public Boolean getValue() {
        return value;
    }

    public void setValue(boolean value) {
        this.value = value;
    }
}
