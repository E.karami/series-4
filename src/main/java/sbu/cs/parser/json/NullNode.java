package sbu.cs.parser.json;

/**
 * this class is for handling null data in Json
 */
public class NullNode extends Node {
    private Object value;

    public NullNode() {
        super.setKey("");
        this.value=null;
    }

    public NullNode(String key ) {
        this.setKey(key);
        this.value=null;
    }

    public NullNode(String key,Object value) {
        super.setKey(key);
        this.value=value;
    }

    @Override
    public String getKey() {
        return super.getKey();
    }

    @Override
    public void setKey(String key) {
        super.setKey(key);
    }

    @Override
    public String getValueString() {
        if(this.value==null){
            return "null";
        }
        return value.toString();
    }

    @Override
    public String toString() {
        return " \"" +this.getKey()+"\" "+
                ": "+
                this.value+
                " ";
    }
    @Override
    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }
}
